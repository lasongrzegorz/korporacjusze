from django.shortcuts import render, redirect, reverse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout


def signup_view(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect(reverse('blog_post_list'))
    else:
        form = UserCreationForm()
    template_name = 'accounts/signup.html'
    context = {'form': form}
    return render(request, template_name, context)


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect(reverse('blog_post_list'))
    else:
        form = AuthenticationForm()
    template_name = 'accounts/login.html'
    context = {'form': form}
    return render(request, template_name, context)


def logout_view(request):
    if request.method == 'POST':
        logout(request)
        return redirect(reverse('blog_post_list'))
