from django.urls import path
from .views import (
    post_delete_view,
    post_detail_view,
    post_list_view,
    post_update_view,
)

# app_name = 'blog'

urlpatterns = [
    path('', post_list_view, name='blog_post_list'),
    path('<str:slug>/', post_detail_view),
    path('<str:slug>/edit/', post_update_view),
    path('<str:slug>/delete/', post_delete_view),
]
