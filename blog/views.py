from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import render, get_object_or_404, redirect
from blog.models import Post
from blog.forms import PostModelForm

from comments.forms import CommentForm
from comments.models import Comment


def post_list_view(request):
    qs = Post.objects.all().published()
    if request.user.is_authenticated:
        user_qs = Post.objects.filter(user=request.user)
        qs = (qs | user_qs).distinct()
    template_name = 'blog/blog.html'
    context = {'object_list': qs}
    print(request)
    return render(request, template_name, context)


@staff_member_required
def post_create_view(request):
    form = PostModelForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.user = request.user
        obj.save()
        form = PostModelForm()
    template_name = 'blog/post_create.html'
    context = {'form': form}
    print(request)
    return render(request, template_name, context)


def post_detail_view(request, slug):
    obj = get_object_or_404(Post, slug=slug)
    template_name = 'blog/post_detail.html'

    initial_data = {
        'content_type': obj.get_content_type,
        'object_id': obj.id,
    }
    comment_form = CommentForm(request.POST or None, initial=initial_data)
    if comment_form.is_valid():
        c_type = comment_form.cleaned_data.get('content_type')
        content_type = ContentType.objects.get(model=c_type)
        obj_id = comment_form.cleaned_data.get('object_id')
        content_data = comment_form.cleaned_data.get('content')

        parent_obj = None
        try:
            parent_id = int(request.POST.get('parent_id'))
        except:
            parent_id = None
        if parent_id:
            parent_qs = Comment.objects.filter(id=parent_id)
            if parent_qs.exists():
                parent_obj = parent_qs.first()

        new_comment, created = Comment.objects.get_or_create(
            user=request.user,
            content_type=content_type,
            object_id=obj_id,
            content=content_data,
            parent=parent_obj,
        )
        return redirect(new_comment.content_object.get_absolute_url())

    comments = Comment.objects.filter_by_instance(obj)
    context = {
        'object': obj,
        'comments': comments,
        'comment_form': comment_form
    }
    return render(request, template_name, context)


@staff_member_required
def post_update_view(request, slug):
    obj = get_object_or_404(Post, slug=slug)
    form = PostModelForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
        context = {'form': form, 'title': f'Post has been updated'}
    else:
        context = {'form': form, 'title': f'Update "{obj.title}"'}
    template_name = 'blog/post_update.html'
    return render(request, template_name, context)


@staff_member_required
def post_delete_view(request, slug):
    obj = get_object_or_404(Post, slug=slug)
    template_name = 'blog/post_delete.html'
    if request.method == 'POST':
        obj.delete()
        return redirect('/blog')
    context = {'object': obj}
    return render(request, template_name, context)
