# Generated by Django 2.2.5 on 2020-03-19 09:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('comments', '0002_auto_20200211_0937'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='comment',
            options={'ordering': ['-timestamp'], 'verbose_name': 'odpowiedz', 'verbose_name_plural': 'odpowiedzi'},
        ),
    ]
