from django.shortcuts import render
from .forms import ContactForm


def index(request):
    return render(request, 'site_general/home.html')


def contact(request):
    form = ContactForm(request.POST or None)
    if form.is_valid():
        print(form.cleaned_data)
        form = ContactForm()
    context = {
        'title': 'Contact us',
        'form': form,
    }
    return render(request, 'site_general/contact_form.html', context)
